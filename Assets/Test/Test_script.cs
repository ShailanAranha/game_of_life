﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Test_script : MonoBehaviour
{
	Color32[] m_colArray1 = new Color32[] { Color.white, Color.red };

	Color32[] m_colArray2 = new Color32[2];


	void test()
	{
		// m_colArray2 = m_colArray1;
        Array.Copy(m_colArray1,m_colArray2, m_colArray1.Length);
		m_colArray1[1] = Color.black;

        Debug.LogError(m_colArray1[1].ToString());
        Debug.LogError(m_colArray2[1].ToString());
	}


	private void Start()
	{
		test();
	}
}
