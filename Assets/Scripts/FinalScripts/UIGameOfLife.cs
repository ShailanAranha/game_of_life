﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Conways_GameOfLife
{
    internal class UIGameOfLife : MonoBehaviour
    {
        /// <summary>
        /// start and stop button object reference
        /// </summary>
        [SerializeField]
        private GameObject m_goStartBtn = null, m_goStopBtn = null;

        /// <summary>
        ///  UI button reference
        /// </summary>
        [SerializeField]
        private Button m_btnStartBtn = null, m_btnDrawBtn = null, m_btnDeleteBtn = null;

        /// <summary>
        /// Sprites for selected and unseleted state of draw button
        /// </summary>
        [SerializeField]
        private Sprite m_drawUnselected = null, m_drawSelected = null;

        /// <summary>
        /// draw icon imgae reference
        /// </summary>
        [SerializeField]
        private Image m_imgDrawIcon = null;

        /// <summary>
        /// DrawSeedLifeforms script reference
        /// </summary>
        [SerializeField]
        private DrawSeedLifeforms m_DrawSeedLifeformsRef = null;

        /// <summary>
        /// To Attach the texture
        /// </summary>
        [SerializeField]
        private RawImage m_rawimgRef = null;

        /// <summary>
        /// Total number of generation completed
        /// </summary>
        [SerializeField]
        private TMP_Text m_txtTotalGenerationCount = null;

        /// <summary>
        /// Confirmation panel
        /// </summary>
        [SerializeField]
        private GameObject m_goConfirmationPanel = null;

        /// <summary>
        /// Instruction panel object
        /// </summary>
        [SerializeField]
        private GameObject m_goInstructionPanel = null;

        /// <summary>
        /// Instruction panel background
        /// </summary>
        [SerializeField]
        private GameObject m_goInstructionBg = null;

        /// <summary>
        /// To set the main texture to the UI
        /// </summary>
        /// <param name="m_tex"></param>
        internal void SetTexture(Texture2D m_tex)
        {
            m_rawimgRef.texture = m_tex;
        }

        /// <summary>
        /// Update total number of generation on UI
        /// </summary>
        /// <param name="a_iTotalGenerationCompleted"></param>
        internal void UpdateTotalGenerationsOnUI(int a_iTotalGenerationCompleted)
        {
            m_txtTotalGenerationCount.text = "Total Generations: " + a_iTotalGenerationCompleted.ToString();
        }

        /// <summary>
        ///  To start the game
        /// </summary>
        public void onClickStartGameBtn()
        {
            DisableInstructionPanel();

            if (m_DrawSeedLifeformsRef.EnableDrawing)
            {
                ForceDisableSeekBtn();
            }

            GameManager.ObjGameOfLife.CalculateSeed();
            GameManager.ObjGameOfLife.IsGenerationsRunning = true;
            m_goStartBtn.SetActive(false);
            m_goStopBtn.SetActive(true);
            m_btnDrawBtn.interactable = m_btnDeleteBtn.interactable = false;
        }

        /// <summary>
        /// To stop the game
        /// </summary>
        public void onClickStopGameBtn()
        {
            GameManager.ObjGameOfLife.IsGenerationsRunning = false;
            m_goStopBtn.SetActive(false);
            m_goStartBtn.SetActive(true);
            m_btnDrawBtn.interactable = m_btnDeleteBtn.interactable = true;
        }

        /// <summary>
        /// Toggles drawing
        /// </summary>
        public void OnClickSeekBtn()
        {
            DisableInstructionPanel();

            m_DrawSeedLifeformsRef.EnableDrawing = !m_DrawSeedLifeformsRef.EnableDrawing;

            if (m_DrawSeedLifeformsRef.EnableDrawing)
            {
                m_imgDrawIcon.sprite = m_drawSelected;
                //m_btnStartBtn.interactable = m_btnDeleteBtn.interactable = false;
            }
            else
            {
                m_imgDrawIcon.sprite = m_drawUnselected;
                //m_btnStartBtn.interactable = m_btnDeleteBtn.interactable = true;
            }
        }

        /// <summary>
        /// Forcefully deactivates the seek button
        /// </summary>
        public void ForceDisableSeekBtn()
        {
            m_DrawSeedLifeformsRef.EnableDrawing = false;
            m_imgDrawIcon.sprite = m_drawUnselected;
        }

        /// <summary>
        /// Reset the entire population(texture)
        /// </summary>
        public void onClickDeleteBtn()
        {
            DisableInstructionPanel();

            GameManager.ObjGameOfLife.DeletePopulation();
        }

        /// <summary>
        /// Close application
        /// </summary>
        public void onClickCloseBtn()
        {
            DisableInstructionPanel();

            m_goConfirmationPanel.SetActive(true);
        }

        /// <summary>
        /// Exits application
        /// </summary>
        public void OnClickYes()
        {
            Application.Quit();
        }

        /// <summary>
        /// Go back to the game
        /// </summary>
        public void OnClickNo()
        {
            m_goConfirmationPanel.SetActive(false);
        }

        /// <summary>
        /// Instruction panel click event
        /// </summary>
        public void OnClickInstructionPanel()
        {
            DisableInstructionPanel();
        }

        /// <summary>
        /// Hides the instruction panel
        /// </summary>
        private void DisableInstructionPanel()
        {
            m_goInstructionPanel.SetActive(false);
            m_goInstructionBg.SetActive(false);
        }
    }
}