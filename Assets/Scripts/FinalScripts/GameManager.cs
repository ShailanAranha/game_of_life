﻿using UnityEngine;

namespace Conways_GameOfLife
{
    internal class GameManager : MonoBehaviour
    {
        /// <summary>
        /// Singleton instance
        /// </summary>
        private static GameManager s_intance = null;

        /// <summary>
        /// Game Of life Object
        /// </summary>
        private GameOfLife m_objGameOfLife = null;
        internal static GameOfLife ObjGameOfLife { get { return s_intance.m_objGameOfLife; } }

        /// <summary>
        /// UI Instance reference
        /// </summary>
        [SerializeField]
        private UIGameOfLife m_UIGameOfLifeRef = null;

        /// <summary>
        /// Privacy policy reference
        /// </summary>
        [SerializeField]
        private PrivacyPolicy m_PrivacyPolicyRef = null;

        /// <summary>
        /// Is Debug?
        /// </summary>
        [SerializeField]
        private bool m_isDebug = false;

        /// <summary>
        /// Is game of life started?
        /// </summary>
        private bool m_isGofStarted = false;

        /// <summary>
        /// Latest privacy policy numver
        /// </summary>
        private const int PRIVACY_POLICY_VERSION = 1;

        /// <summary>
        /// Privacy policy primary key
        /// </summary>
        private const string PRIVACY_POLICY_KEY = "PRIVACY_POLICY_KEY";

        /// <summary>
        /// Monobehaviour awake 
        /// </summary>
        private void Awake()
        {
            s_intance = this;
        }

        /// <summary>
        /// Start is called before the first frame update
        /// </summary>
        void Start()
        {
            if (PlayerPrefs.HasKey(PRIVACY_POLICY_KEY))
            {
                int l_version = PlayerPrefs.GetInt(PRIVACY_POLICY_KEY);

                if (l_version < PRIVACY_POLICY_VERSION)
                {
                    m_PrivacyPolicyRef.ShowPrivacyPolicy();
                }
                else
                {
                    StartGameOfLife();
                }
            }
            else
            {
                //show privacy policy
                m_PrivacyPolicyRef.ShowPrivacyPolicy();
            }
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        void Update()
        {
            if (!m_isGofStarted)
            {
                return;
            }

            m_objGameOfLife.IterateGenerations(Time.deltaTime);
        }

        /// <summary>
        /// Monobehaviour destroy
        /// </summary>
        private void OnDestroy()
        {
            m_objGameOfLife.OnGenerationComplete -= m_UIGameOfLifeRef.UpdateTotalGenerationsOnUI;
            s_intance = null;
        }

        /// <summary>
        /// Set initial values and Initiate Game of Life
        /// </summary>
        private void StartGameOfLife()
        {
            m_objGameOfLife = new GameOfLife(m_isDebug);
            m_UIGameOfLifeRef.SetTexture(GameManager.ObjGameOfLife.TextureUniverse);
            m_objGameOfLife.OnGenerationComplete -= m_UIGameOfLifeRef.UpdateTotalGenerationsOnUI;
            m_objGameOfLife.OnGenerationComplete += m_UIGameOfLifeRef.UpdateTotalGenerationsOnUI;
            m_isGofStarted = true;
        }

        /// <summary>
        /// Update the accepted version of privacy policy
        /// </summary>
        internal static void SetAcceptedPrivacyPolicyVersion()
        {
            PlayerPrefs.SetInt(PRIVACY_POLICY_KEY, PRIVACY_POLICY_VERSION);
        }

        /// <summary>
        /// Iniate game og life
        /// </summary>
        internal static void InitiateGameOfLife()
        {
            s_intance.StartGameOfLife();
        }
    }
}