﻿// Conway's game of life

using System;
using UnityEngine;

namespace Conways_GameOfLife
{
    internal class GameOfLife //: MonoBehaviour
    {
        /// <summary>
        /// For creation of cellular structure
        /// </summary>
        internal Texture2D TextureUniverse { get; private set; }

        /// <summary>
        /// Is generations iterating?
        /// </summary>
        /// <value></value>
        internal bool IsGenerationsRunning { get; set; } = false;

        /// <summary>
        /// Total generations completed so far(1 second = 1 generation)
        /// </summary>
        /// <returns></returns>
        internal int TotalGenerationCompleted { get { return (int)m_fltCurrentTimeInSec; } }

        /// <summary>
        /// Invoke whern one genenartion is completed
        /// </summary>
        internal event Action<int> OnGenerationComplete = null;

        /// <summary>
        /// Holds life status of current and next generation
        /// </summary>
        private Color32[] m_colCurrentGenerationArray = null, m_colNextGenerationArray = null;

        /// <summary>
        /// Keeps track of time in seconds for iteration 
        /// </summary>
        private float m_fltCurrentTimeInSec = 0.0f;

        /// <summary>
        /// Holds cache data
        /// </summary>
        private int m_iCurrentTimeCache = 0, m_iPreviousTimeInSec = 0;

        /// <summary>
        /// For Testing
        /// </summary>
        private bool m_IsDebug = false;

        /// <summary>
        /// Default constructor
        /// </summary>
        internal GameOfLife()
        {
            CreateUniverse();
        }

        /// <summary>
        /// Assign debug value
        /// </summary>
        /// <param name="a_isDebug"></param>
        internal GameOfLife(bool a_isDebug)
        {
            m_IsDebug = a_isDebug;
            CreateUniverse();
        }

        #region CALCULATE INITIATE SEED AND DELETE ALL POPULATION

        /// <summary>
        /// Calculate initial population of the universe
        /// </summary>
        internal void CalculateSeed()
        {
            m_colCurrentGenerationArray = TextureUniverse.GetPixels32();
            m_colNextGenerationArray = new Color32[m_colCurrentGenerationArray.Length];
            Array.Copy(m_colCurrentGenerationArray, m_colNextGenerationArray, m_colCurrentGenerationArray.Length);
        }

        /// <summary>
        /// Deletes entire population(resets the universe)
        /// </summary>
        internal void DeletePopulation()
        {
            for (int i_index = 0; i_index < m_colCurrentGenerationArray.Length; i_index++)
            {
                m_colCurrentGenerationArray[i_index] = ChangeLifeStatus(ELifeStatus.dead);
            }

            TextureUniverse.SetPixels32(m_colCurrentGenerationArray);
            TextureUniverse.Apply();

            m_colNextGenerationArray = null;
            m_fltCurrentTimeInSec = 0.0f;
            m_iCurrentTimeCache = m_iPreviousTimeInSec = 0;

            OnGenerationComplete?.Invoke(TotalGenerationCompleted);

            if (m_IsDebug)
                Debug.Log("Population deleted!!");
        }
        #endregion

        #region ITERATE GENERATIONS

        /// <summary>
        /// Undergo multiple generations where 1 generation = 1 second
        /// </summary>
        internal void IterateGenerations(float a_fltDeltaTime)
        {
            if (!IsGenerationsRunning)
            {
                return;
            }

            m_fltCurrentTimeInSec += a_fltDeltaTime;
            m_iCurrentTimeCache = (int)m_fltCurrentTimeInSec;

            if (m_iCurrentTimeCache > m_iPreviousTimeInSec)
            {
                CompleteOneGeneration();
            }

            m_iPreviousTimeInSec = (int)m_fltCurrentTimeInSec;
        }

        #endregion

        #region CREATE TEXTURE

        /// <summary>
        /// Creates 2D Texture
        /// </summary>
        private void CreateUniverse()
        {
            TextureUniverse = new Texture2D(32, 64, TextureFormat.ARGB32, false);
            TextureUniverse.filterMode = 0;
            // m_rawimg.texture = TextureUniverse;
            // m_rawimg.SetNativeSize();

            m_colCurrentGenerationArray = TextureUniverse.GetPixels32();

            for (int i_index = 0; i_index < m_colCurrentGenerationArray.Length; i_index++)
            {
                m_colCurrentGenerationArray[i_index] = ChangeLifeStatus(ELifeStatus.dead);
            }

            TextureUniverse.SetPixels32(m_colCurrentGenerationArray);
            TextureUniverse.Apply();

            if (m_IsDebug)
                Debug.Log("Texture Created!!");

        }

        #endregion

        #region RULES, STATUS AND RESULT

        /// <summary>
        /// Undergo one generation.
        /// </summary>
        private void CompleteOneGeneration()
        {
            int l_count = m_colCurrentGenerationArray.Length; //caching
            for (int i_index = 0; i_index < l_count; i_index++)
            {
                int l_intNeighboringStatus = GetLifeStatusOfNeighboringPixels(i_index);
                DecideStatusOfCurrentPixel(l_intNeighboringStatus, i_index);
            }

            TextureUniverse.SetPixels32(m_colNextGenerationArray);

            Array.Copy(m_colNextGenerationArray, m_colCurrentGenerationArray, m_colNextGenerationArray.Length);

            TextureUniverse.Apply();

            OnGenerationComplete?.Invoke(TotalGenerationCompleted);

            if (m_IsDebug)
                Debug.Log("Geneation completed!!");

        }

        /// <summary>
        ///  finding the life status of the neighbouring cells
        /// </summary>
        /// <param name="a_intPixelNumber">current cell</param>
        /// <returns>number of neighbouring cells alive</returns>
        private int GetLifeStatusOfNeighboringPixels(int a_intPixelNumber)
        {
            if (a_intPixelNumber > (TextureUniverse.width * TextureUniverse.height))
            {
                Debug.LogError(a_intPixelNumber + ":Unexpected pixel number found!!!!");
            }

            int l_iRow;
            int l_iCol;

            if (a_intPixelNumber < TextureUniverse.width)
            {
                l_iRow = 0;
                l_iCol = a_intPixelNumber;
            }
            else
            {
                l_iRow = a_intPixelNumber / TextureUniverse.width;
                l_iCol = a_intPixelNumber % TextureUniverse.width;
            }

            if (m_IsDebug)
                Debug.Log("Pixel Number::" + a_intPixelNumber + " l_row:" + l_iRow + " col:" + l_iCol);



            Color32 l_topLeft, l_top, l_topRight, l_Left, l_right, l_botLeft, l_botRight, l_bot;

            if (l_iRow + 1 > TextureUniverse.height - 1 || l_iCol - 1 < 0)
            {
                l_topLeft = ChangeLifeStatus(ELifeStatus.dead);

                if (m_IsDebug)
                    Debug.Log("l_topLeft::" + " l_row:" + l_iRow + " l_col:" + l_iCol + " LifeStatus:" + CheckLifeStatusToString(l_topLeft));
            }
            else
            {
                l_topLeft = m_colCurrentGenerationArray[GetRequiredPixel(l_iRow + 1, l_iCol - 1)];

                if (m_IsDebug)
                    Debug.LogError("l_topLeft" + GetRequiredPixel(l_iRow + 1, l_iCol - 1) + " LifeStatus:" + CheckLifeStatusToString(l_topLeft));

            }

            if (l_iRow + 1 > TextureUniverse.height - 1)
            {
                l_top = ChangeLifeStatus(ELifeStatus.dead);

                if (m_IsDebug)
                    Debug.Log("l_top:" + " l_row:" + l_iRow + " l_col:" + l_iCol + " LifeStatus:" + CheckLifeStatusToString(l_top));

            }

            else
            {
                l_top = m_colCurrentGenerationArray[GetRequiredPixel(l_iRow + 1, l_iCol)];

                if (m_IsDebug)
                    Debug.Log("l_top" + GetRequiredPixel(l_iRow + 1, l_iCol) + " LifeStatus:" + CheckLifeStatusToString(l_top));

            }

            if (l_iRow + 1 > TextureUniverse.height - 1 || l_iCol + 1 > TextureUniverse.width - 1)
            {
                l_topRight = ChangeLifeStatus(ELifeStatus.dead);

                if (m_IsDebug)
                    Debug.Log("l_topRight:" + " l_row:" + l_iRow + " l_col:" + l_iCol + " LifeStatus:" + CheckLifeStatusToString(l_topRight));
            }
            else
            {
                l_topRight = m_colCurrentGenerationArray[GetRequiredPixel(l_iRow + 1, l_iCol + 1)];

                if (m_IsDebug)
                    Debug.LogError("l_topRight" + GetRequiredPixel(l_iRow + 1, l_iCol + 1) + " LifeStatus:" + CheckLifeStatusToString(l_topRight));

            }

            if (l_iCol - 1 < 0)
            {
                l_Left = ChangeLifeStatus(ELifeStatus.dead);

                if (m_IsDebug)
                    Debug.Log("l_Left:" + " l_row:" + l_iRow + " l_col:" + l_iCol + " LifeStatus:" + CheckLifeStatusToString(l_Left));


            }
            else
            {
                l_Left = m_colCurrentGenerationArray[GetRequiredPixel(l_iRow, l_iCol - 1)];

                if (m_IsDebug)
                    Debug.Log("l_Left" + GetRequiredPixel(l_iRow, l_iCol - 1) + " LifeStatus:" + CheckLifeStatusToString(l_Left));

            }


            if (l_iCol + 1 > TextureUniverse.width - 1)
            {
                l_right = ChangeLifeStatus(ELifeStatus.dead);

                if (m_IsDebug)
                    Debug.Log("l_right:" + " l_row:" + l_iRow + " l_col:" + l_iCol + " LifeStatus:" + CheckLifeStatusToString(l_right));


            }

            else
            {
                l_right = m_colCurrentGenerationArray[GetRequiredPixel(l_iRow, l_iCol + 1)];

                if (m_IsDebug)
                    Debug.Log("l_right" + GetRequiredPixel(l_iRow, l_iCol + 1) + " LifeStatus:" + CheckLifeStatusToString(l_right));


            }

            if (l_iRow - 1 < 0 || l_iCol - 1 < 0)
            {
                l_botLeft = ChangeLifeStatus(ELifeStatus.dead);

                if (m_IsDebug)
                    Debug.Log("l_botLeft:" + " l_row:" + l_iRow + " l_col:" + l_iCol + " LifeStatus:" + CheckLifeStatusToString(l_botLeft));

            }

            else
            {
                l_botLeft = m_colCurrentGenerationArray[GetRequiredPixel(l_iRow - 1, l_iCol - 1)];

                if (m_IsDebug)
                    Debug.Log("l_botLeft" + GetRequiredPixel(l_iRow - 1, l_iCol - 1) + " LifeStatus:" + CheckLifeStatusToString(l_botLeft));


            }

            if (l_iRow - 1 < 0)
            {
                l_botRight = ChangeLifeStatus(ELifeStatus.dead);

                if (m_IsDebug)
                    Debug.Log("l_botRight:" + " l_row:" + l_iRow + " l_col:" + l_iCol + " LifeStatus:" + CheckLifeStatusToString(l_botRight));


            }

            else
            {
                l_botRight = m_colCurrentGenerationArray[GetRequiredPixel(l_iRow - 1, l_iCol)];

                if (m_IsDebug)
                    Debug.Log("l_botRight" + GetRequiredPixel(l_iRow - 1, l_iCol) + " LifeStatus:" + CheckLifeStatusToString(l_botRight));


            }

            if (l_iRow - 1 < 0 || l_iCol + 1 > TextureUniverse.width - 1)
            {
                l_bot = ChangeLifeStatus(ELifeStatus.dead);

                if (m_IsDebug)
                    Debug.Log("l_bot:" + " l_row:" + l_iRow + " l_col:" + l_iCol + " LifeStatus:" + CheckLifeStatusToString(l_bot));


            }
            else
            {
                l_bot = m_colCurrentGenerationArray[GetRequiredPixel(l_iRow - 1, l_iCol + 1)];

                if (m_IsDebug)
                    Debug.Log("l_bot" + GetRequiredPixel(l_iRow - 1, l_iCol + 1) + " LifeStatus:" + CheckLifeStatusToString(l_bot));


            }

            return (CheckLifeStatus(l_topLeft) + CheckLifeStatus(l_top) + CheckLifeStatus(l_topRight) +
                    CheckLifeStatus(l_Left) + CheckLifeStatus(l_right) + CheckLifeStatus(l_botLeft) +
                    CheckLifeStatus(l_botRight) + CheckLifeStatus(l_bot));


        }

        /// <summary>
        /// Find the target pixel using the row and column value { (row*width)+column }
        /// </summary>
        /// <param name="a_row">Row of the texture</param>
        /// <param name="a_col">Column of the texture</param>
        /// <returns>target pixel number in int</returns>
        private int GetRequiredPixel(int a_row, int a_col)
        {
            return ((a_row * TextureUniverse.width) + a_col);
        }

        /// <summary>
        /// decide the life status of the current cell based on all the rules
        /// </summary>
        /// <param name="a_intNeigboringStatus">no. of neighbouring cells alive</param>
        /// <param name="a_intCurrentPixelIndex">current cell</param>
        private void DecideStatusOfCurrentPixel(int a_intNeigboringStatus, int a_intCurrentPixelIndex)
        {
            // if (a_intNeigboringStatus > 1)
            // 	Debug.Log("a_intNeigboringStatus:" + a_intNeigboringStatus + " a_intCurrentPixelIndex:" + a_intCurrentPixelIndex);

            if (a_intNeigboringStatus < 2 && (CheckLifeStatus(m_colCurrentGenerationArray[a_intCurrentPixelIndex]) == 1))
            {
                UnderPopulation(a_intCurrentPixelIndex);
            }

            else if (a_intNeigboringStatus == 2 && (CheckLifeStatus(m_colCurrentGenerationArray[a_intCurrentPixelIndex]) == 1))
            {
                NextGeneration(a_intCurrentPixelIndex);
            }

            else if (a_intNeigboringStatus > 3 && (CheckLifeStatus(m_colCurrentGenerationArray[a_intCurrentPixelIndex]) == 1))
            {
                OverPopulation(a_intCurrentPixelIndex);
            }

            else if (a_intNeigboringStatus == 3)
            {
                if (CheckLifeStatus(m_colCurrentGenerationArray[a_intCurrentPixelIndex]) == 1)
                {
                    NextGeneration(a_intCurrentPixelIndex);
                }
                else
                {
                    Reproduction(a_intCurrentPixelIndex);
                }
            }
        }

        /// <summary>
        /// Any live cell with fewer than two live neighbours dies, as if by underpopulation.
        /// </summary>
        /// <param name="a_intCurrentPixelIndex">current cell</param>
        private void UnderPopulation(int a_intCurrentPixelIndex)
        {
            m_colNextGenerationArray[a_intCurrentPixelIndex] = ChangeLifeStatus(ELifeStatus.dead);

            if (m_IsDebug)
                Debug.Log("Pixel Number(cell):" + a_intCurrentPixelIndex + " is UnderPopulation");
        }

        /// <summary>
        /// Any live cell with more than three live neighbours dies, as if by overpopulation.
        /// </summary>
        /// <param name="a_intCurrentPixelIndex">current cell</param>
        private void OverPopulation(int a_intCurrentPixelIndex)
        {
            m_colNextGenerationArray[a_intCurrentPixelIndex] = ChangeLifeStatus(ELifeStatus.dead);

            if (m_IsDebug)
                Debug.Log("Pixel Number(cell):" + a_intCurrentPixelIndex + " is OverPopulation");
        }

        /// <summary>
        /// Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
        /// </summary>
        /// <param name="a_intCurrentPixelIndex">current cell</param>
        private void Reproduction(int a_intCurrentPixelIndex)
        {
            m_colNextGenerationArray[a_intCurrentPixelIndex] = ChangeLifeStatus(ELifeStatus.alive);

            if (m_IsDebug)
                Debug.Log("Pixel Number(cell):" + a_intCurrentPixelIndex + " is under Reproduction");

        }

        /// <summary>
        /// Any live cell with two or three live neighbours lives on to the next generation.
        /// </summary>
        /// <param name="a_intCurrentPixelIndex">current cell</param>
        private void NextGeneration(int a_intCurrentPixelIndex)
        {
            m_colNextGenerationArray[a_intCurrentPixelIndex] = ChangeLifeStatus(ELifeStatus.alive);

            if (m_IsDebug)
                Debug.Log("Pixel Number(cell):" + a_intCurrentPixelIndex + " goes to NextGeneration");
        }

        #endregion

        #region ALL HELPER FUNCTIONS

        /// <summary>
        /// change life status of the cell
        /// </summary>
        /// <param name="a_LifeStatus"></param>
        /// <returns></returns>
        private Color32 ChangeLifeStatus(ELifeStatus a_LifeStatus)
        {
            switch (a_LifeStatus)
            {
                case ELifeStatus.alive:

                    return Color.black;

                case ELifeStatus.dead:

                    return Color.white;

                default:
                    Debug.LogError("unexpected life status found!!!");
                    return Color.clear;
            }
        }

        /// <summary>
        ///  Check life status of the cell
        /// </summary>
        /// <param name="a_color">color of the cell</param>
        /// <returns>life value in string</returns>
        private int CheckLifeStatus(Color32 a_color)
        {
            // if (a_color.Equals(Color.black))
            if (a_color == Color.black)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        ///  Check life status of the cell
        /// </summary>
        /// <param name="a_color">color of the cell</param>
        /// <returns>life value in string</returns>
        private string CheckLifeStatusToString(Color32 a_color)
        {
            // if (a_color.Equals(Color.black))
            if (a_color == Color.black)
            {
                return "Alive";
            }
            else
            {
                return "Dead";
            }
        }
        #endregion

    }

    /// <summary>
    /// Life Status
    /// </summary>
    internal enum ELifeStatus
    {
        alive = 0,
        dead = 1
    }

}



