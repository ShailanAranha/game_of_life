﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Conways_GameOfLife
{
    internal class DrawSeedLifeforms : MonoBehaviour, IDragHandler, IPointerDownHandler
    {
        /// <summary>
        /// Raw image instance reference
        /// </summary>
        [SerializeField]
        private RawImage m_imgMainRef = null;

        /// <summary>
        /// Main texture instance reference
        /// </summary>
        private Texture2D m_texMainTexture = null;
        private Texture2D MainTexture
        {
            get
            {
                if (m_texMainTexture == null)
                {
                    m_texMainTexture = (Texture2D)m_imgMainRef.texture;
                }
                return m_texMainTexture;
            }
            set
            {
                m_texMainTexture = value;
            }
        }

        /// <summary>
        /// Main camera reference
        /// </summary>
        [SerializeField]
        private Camera m_camMainRef = null;

        /// <summary>
        /// Is Drawing enabled?
        /// </summary>
        /// <value></value>
        internal bool EnableDrawing { get; set; } = false;

        /// <summary>
        /// Is Debug?
        /// </summary>
        [SerializeField]
        private bool m_isDebug = false;

        /// <summary>
        /// Used to populates initial population
        /// </summary>
        private void DrawPopulation()
        {
            Vector3 l_vecMousePos = Input.mousePosition;
            l_vecMousePos.z = 10;
            Vector3 l_vecScreenPos = m_camMainRef.ScreenToWorldPoint(l_vecMousePos);
            RaycastHit2D l_hit = Physics2D.Raycast(l_vecMousePos, Vector2.zero);
            Vector2 l_vecUV;

            if (l_hit.collider == null)
            {
                return;
            }

            l_vecUV.x = (l_hit.point.x - l_hit.collider.bounds.min.x) / l_hit.collider.bounds.size.x;
            l_vecUV.y = (l_hit.point.y - l_hit.collider.bounds.min.y) / l_hit.collider.bounds.size.y;

            int l_iX = (int)(l_vecUV.x * MainTexture.width);
            int l_iY = (int)(l_vecUV.y * MainTexture.height);

            if (m_isDebug)
                Debug.Log("X:" + l_iX + " Y:" + l_iY);

            MainTexture.SetPixel(l_iX, l_iY, Color.black);
            MainTexture.Apply();
        }

        /// <summary>
        /// UI Drag event
        /// </summary>
        /// <param name="eventData"></param>
        public void OnDrag(PointerEventData eventData)
        {
            if (EnableDrawing)
            {
                DrawPopulation();
            }
        }

        /// <summary>
        /// UI Pointer down event
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerDown(PointerEventData eventData)
        {
            if (EnableDrawing)
            {
                DrawPopulation();
            }
        }
    }
}