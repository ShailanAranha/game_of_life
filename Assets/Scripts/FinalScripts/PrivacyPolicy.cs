using UnityEngine;

namespace Conways_GameOfLife
{
    internal class PrivacyPolicy : MonoBehaviour
    {
        /// <summary>
        /// Privacy policy parent object
        /// </summary>
        [SerializeField]
        private GameObject m_goPrivacyPolicyBox = null;

        /// <summary>
        /// Privacy policy confirmatin box object
        /// </summary>
        [SerializeField]
        private GameObject m_goPrivacyPolicyConfirmationBax = null;

        /// <summary>
        /// Privacy policy bg
        /// </summary>
        [SerializeField]
        private GameObject m_goPrivacyPolicyBg = null;

        /// <summary>
        /// On Click event for accept button
        /// </summary>
        public void OnClick_AcceptBtn()
        {
            GameManager.SetAcceptedPrivacyPolicyVersion();
            HidePrivacyPolicy();
            GameManager.InitiateGameOfLife();
        }

        /// <summary>
        /// On Click event for reject button
        /// </summary>
        public void OnClick_RejectBtn()
        {
            m_goPrivacyPolicyConfirmationBax.SetActive(true);
            m_goPrivacyPolicyBox.SetActive(false);
        }

        /// <summary>
        /// On Click event for click to read btn
        /// </summary>
        public void OnClick_ClickToReadBtn()
        {
            Application.OpenURL("https://conways-game-of-life.blogspot.com/2022/02/conways-game-of-life-privacy-policy.html");
        }

        /// <summary>
        /// On Click event for back button
        /// </summary>
        public void OnClick_BackBtn()
        {
            m_goPrivacyPolicyBox.SetActive(true);
            m_goPrivacyPolicyConfirmationBax.SetActive(false);
        }

        /// <summary>
        /// On Click event for exit button
        /// </summary>
        public void OnClick_ExitBtn()
        {
            Application.Quit();
        }

        /// <summary>
        /// Show privacy policy UI
        /// </summary>
        public void ShowPrivacyPolicy()
        {
            gameObject.SetActive(true);
            m_goPrivacyPolicyBg.SetActive(true);
        }

        /// <summary>
        /// Hide privacy policy UI
        /// </summary>
        public void HidePrivacyPolicy()
        {
            gameObject.SetActive(false);
            m_goPrivacyPolicyBg.SetActive(false);
        }
    }
}